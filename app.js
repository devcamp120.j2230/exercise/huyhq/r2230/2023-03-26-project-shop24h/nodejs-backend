const express = require("express");
const mongoose = require("mongoose");

const app = express();
app.use(express.json());

//get model
const productTypeModel = require("./app/model/ProductType");
const productModel = require("./app/model/Product");
const customerModel = require("./app/model/Customer");
const orderModel = require("./app/model/Order");
const orderDetailModel = require("./app/model/OrderDetail");
//get route
const {productTypeRouter} = require("./app/routes/productTypeRoute");
const {productRouter} = require("./app/routes/productRoute");
const {customerRouter} = require("./app/routes/customerRoute");
const {orderRouter} = require("./app/routes/orderRoute");
const {orderDetailRouter} = require("./app/routes/orderDetailRoute");
const {loginRoute} = require("./app/routes/loginRoute");
const {groupRouter} = require("./app/routes/groupRoute");
const {authorizationRoute} = require("./app/routes/authorizationRoute");

//connect mongoDb
const nameDB = "CRUD_Shop24h";
mongoose.connect("mongodb://127.0.0.1:27017/"+nameDB, (error)=>{
    if (error) throw error;
    console.log('Successfully connected to DB: ' + nameDB);
})

const port = 8000;
app.get("/", (req, res)=>{
    return res.status(200).json({
        message: "Connect success!"
    })
})

app.use(function (req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, auth-token');
	res.setHeader('Access-Control-Allow-Credentials', true);
	next(); 
});

app.use("/", productTypeRouter);
app.use("/", productRouter);
app.use("/", customerRouter);
app.use("/", orderRouter);
app.use("/", orderDetailRouter);
app.use("/", loginRoute);
app.use("/", authorizationRoute);
app.use("/", groupRouter);

app.listen(port, 'localhost',()=>{
    console.log("Connect to port: "+port);
})

