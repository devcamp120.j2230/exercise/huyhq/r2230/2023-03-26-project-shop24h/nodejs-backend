const jwt = require('jsonwebtoken');

module.exports = (request, response, next) => {
    const token = request.header('auth-token');
    
    if (!token) return response.status(401).json({
        error: `Error 401.`,
        message: `Access Denied!`
    });

    try {
        const verified = jwt.verify(token, 'admin');
        next();
    } catch (err) {
        return response.status(400).json({
            error: `Error 400.`,
            message: `User is not access!`
        });;
    }
};