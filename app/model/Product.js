const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const productSchema = new Schema({
    _id: mongoose.Types.ObjectId,
	name: {
        type: String, 
        unique: true, 
        required: true
    },
	description: String,
	type: {
        type: mongoose.Types.ObjectId, 
        ref: "productType", 
        required: true
    },
	imageUrl: {
        type: String, 
        required: true
    },
	color: {
        type: String, 
        required: true
    },
	buyPrice: {
        type: Number, 
        required: true
    },
	promotionPrice: {
        type: Number, 
        required: true
    },
	amount: {
        type: Number, 
        default: 0
    },
    dateCreated: {
        type: Date,
    },
    dateUpdated:{
        type: Date,
        default: Date.now(),
    },
    isDeleted: {
        type: Boolean, 
        default: false
    },
    deletedAt: {
        type: Date,
        default: "",
    }
});

module.exports = mongoose.model("product", productSchema)