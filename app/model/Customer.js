const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const customerSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    fullName: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },

    hash: { type: String },
    salt: { type: String },

    imageAvatar: {
        type: String,
        default: ""
    },
    address: {
        type: String,
        default: ""
    },
    district: {
        type: String,
        default: ""
    },
    city: {
        type: String,
        default: ""
    },
    country: {
        type: String,
        default: ""
    },
    
    isDeleted: {
        type: Boolean,
        default: false
    },
    deletedAt: {
        type: Date,
        default: "",
    },
    group: {
        type: mongoose.Types.ObjectId,
        ref: "group",
    },
    order: [{
        type: mongoose.Types.ObjectId,
        ref: "order",
    }],
    dateCreated: {
        type: Date,
    },
    dateUpdated:{
        type: Date,
        default: Date.now(),
    },
});

module.exports = mongoose.model("customer", customerSchema)