const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const orderSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    orderDate: {
        type: Date,
        default: Date.now()
    },
    shippedDate: {
        type: Date,
    },
    status: {
        type: String,
        require: true
    },
	costShip: {
        type: Number,
        default: 0
    },
                        subtotal: {
        type: Number,
        default: 0
    },
                        discount: {
        type: Number,
        default: 0
    },
                        total: {
        type: Number,
        default: 0
    },
    note: String,
    customer: {
        type: mongoose.Types.ObjectId,
        ref: "customer",
    },
    orderDetail: [{
        type: mongoose.Types.ObjectId,
        ref: "orderDetail",
    }],
});

module.exports = mongoose.model("order", orderSchema)