const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const productTypeSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    name: {
        type: String, 
        unique: true, 
        require: true
    },
	description: {
        type: String
    }
});

module.exports = mongoose.model("productType", productTypeSchema)