const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const groupSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    name: {
        type: String,
        require: true
    },
    description: {
        type: String, 
    }
});

module.exports = mongoose.model("group", groupSchema)