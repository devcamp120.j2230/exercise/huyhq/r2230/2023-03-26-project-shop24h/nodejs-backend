const express = require("express");
const verifyToken = require("../middleware/verifyToken");

const {
    postAProduct,
    getAllProduct,
    getAProductById,
    putAProductById,
    deleteAProductById,
    softDeleteProduct,
    removeSoftDeleteProduct
} = require("../controller/productController");

const productRouter = express.Router();

//productRouter.get("/product", verifyToken, getAllProduct);
productRouter.get("/product", getAllProduct);
productRouter.post("/product", verifyToken, postAProduct);
productRouter.get("/product/:id", getAProductById);
productRouter.put("/product/:id", verifyToken, putAProductById);
productRouter.delete("/product/:id", verifyToken, deleteAProductById);
productRouter.put("/product/soft-delete/:id", verifyToken, softDeleteProduct);
productRouter.put("/product/soft-remove/:id", removeSoftDeleteProduct);

module.exports = {productRouter};
