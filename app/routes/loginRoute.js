const express = require("express");

const {
    registerNewUser,
    loginUser,
updateUser,
    logoutUser,
} = require("../controller/loginController");

const loginRoute = express.Router();

loginRoute.post("/register", registerNewUser);
loginRoute.post("/login", loginUser);
loginRoute.post("/login-update", updateUser);
loginRoute.post("/logout", logoutUser);

module.exports = {loginRoute};
