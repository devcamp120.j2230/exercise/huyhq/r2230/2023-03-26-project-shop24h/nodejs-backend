const express = require("express");

const {
    postAGroup,
    getAllGroup,
    getAGroupById,
    putAGroupById,
    deleteAGroupById
} = require("../controller/groupController");

const groupRouter = express.Router();

groupRouter.get("/group", getAllGroup);
groupRouter.post("/group", postAGroup);
groupRouter.get("/group/:id", getAGroupById);
groupRouter.put("/group/:id", putAGroupById);
groupRouter.delete("/group/:id", deleteAGroupById);

module.exports = {groupRouter};
