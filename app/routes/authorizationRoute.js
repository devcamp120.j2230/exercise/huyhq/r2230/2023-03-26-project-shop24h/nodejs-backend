const express = require("express");

const {
    authorization
} = require("../controller/authorizationController");

const authorizationRoute = express.Router();

authorizationRoute.get("/authorization", authorization);

module.exports = {authorizationRoute};
