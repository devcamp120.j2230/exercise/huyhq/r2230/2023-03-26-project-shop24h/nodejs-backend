const express = require("express");

const {
    postACustomer,
    getAllCustomer,
    getACustomerById,
    putACustomerById,
    deleteACustomerById,
    softDeleteCustomer,
    removeSoftDeleteCustomer
} = require("../controller/customerController");

const customerRouter = express.Router();
const verifyToken = require("../middleware/verifyToken");

//customerRouter.get("/customer", verifyToken, getAllCustomer);
customerRouter.get("/customer", verifyToken, getAllCustomer);
customerRouter.post("/customer", verifyToken, postACustomer);
customerRouter.get("/customer/:id", getACustomerById);
customerRouter.put("/customer/:id", putACustomerById);
customerRouter.put("/customer/soft-delete/:id", verifyToken, softDeleteCustomer);
customerRouter.put("/customer/soft-remove/:id", removeSoftDeleteCustomer);
customerRouter.delete("/customer/:id", verifyToken, deleteACustomerById);

module.exports = {customerRouter};
