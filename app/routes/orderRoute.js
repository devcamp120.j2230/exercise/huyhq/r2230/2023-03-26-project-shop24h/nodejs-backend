const express = require("express");
const verifyToken = require("../middleware/verifyToken");

const {
    postAOrderOfCustomer,
    getAllOrderOfCustomer,
    getAllOrder,
    getAOrderById,
    putAOrderById,
    deleteAOrderById
} = require("../controller/orderController");

const orderRouter = express.Router();

orderRouter.get("/customer/:customerId/order", verifyToken, getAllOrderOfCustomer);
orderRouter.post("/customer/:customerId/order", postAOrderOfCustomer);
orderRouter.get("/order", verifyToken, getAllOrder);
orderRouter.get("/order/:orderId", getAOrderById);
orderRouter.put("/order/:orderId", verifyToken, putAOrderById);
orderRouter.delete("/customer/:customerId/order/:orderId", verifyToken, deleteAOrderById);

module.exports = {orderRouter};
