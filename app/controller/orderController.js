const { default: mongoose } = require("mongoose");

const orderModel = require("../model/Order");
const orderDetailModel = require("../model/OrderDetail");
const customerModel = require("../model/Customer");
const productModel = require("../model/Product");
const productTypeModel = require("../model/ProductType");

const postAOrderOfCustomer = async (req, res) => {
    var customerId = req.params.customerId;

    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            message: "Loi 400: Id Customer khong dung."
        })
    }

    //tạo mới order
    var order = req.body.order;
    var orderId = mongoose.Types.ObjectId();
    var newOrder = new orderModel({
        _id: orderId,
        shippedDate: order.shippedDate,
        note: order.note,
        status: "open",
	costShip: order.costShip,
        subtotal: order.subtotal,
        discount: order.discount,
        total: order.total,
        customer: customerId
    })

    orderModel.create(newOrder, async (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            customerModel.findByIdAndUpdate(customerId,
                {
                    $push: { order: data._id },
		    dateUpdated: Date.now(),
                },
                (err) => {
                    if (err) {
                        return res.status(500).json({
                            message: `Loi 500: ${error.message}.`
                        });
                    }
                }
            )

        };
    });

    //tạo mới order detail
    var details = req.body.detail;
    details.forEach(detail => {
        var newOrderDetail = new orderDetailModel({
            _id: mongoose.Types.ObjectId(),
            product: detail.product,
            quantity: detail.quantity,
        })

        orderDetailModel.create(newOrderDetail, async (error, data) => {
            if (error) {
                return res.status(500).json({
                    message: `Loi 500: ${error.message}.`
                });
            } else {
                orderModel.findByIdAndUpdate(orderId,
                    {
                        $push: { orderDetail: data._id }
                    },
                    (err) => {
                        if (err) {
                            return res.status(500).json({
                                message: `Loi 500: ${error.message}.`
                            });
                        }
                    }
                )
            };
        });
    });

    return res.status(201).json({
        message: `Tao don hang thanh cong.`,
    });
};

const getAllOrderOfCustomer = (req, res) => {
    var customerId = req.params.customerId;

    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            message: "Loi 400: Id Customer khong dung."
        })
    };

    customerModel.findById(customerId)
        .populate("order")
        .exec((error, data) => {
            if (error) {
                return res.status(500).json({
                    message: `Loi 500: ${error.message}.`
                });
            } else {
                return res.status(200).json({
                    message: `Lay thong tin thanh cong.`,
                    order: data.order
                });
            };
        })
};

const getAllOrder = (req, res) => {
    var start = req.query.start;
    var limitOrder = req.query.limit;

var order = req.query.order;
var orderBy = req.query.orderBy;

//thu thập dữ liệu trên front-end
    let nameProduct = req.query.product;

//tạo ra điều kiện lọc
    let condition = {};

    if (nameProduct) {
        condition.name = {$regex: nameProduct};
    }
	console.log(condition)

	var sort = "";
	sort = order ? {[`${orderBy}`]: order} : {orderDate: 'desc'}

    orderModel.find()
        .populate(
            { path: 'orderDetail', model: orderDetailModel, populate: { path: 'product', model: productModel, populate: {path: 'type', model: productTypeModel}} }
        )
        .populate(
            { path: 'customer', model: customerModel }
        )
.sort(sort)
        .skip(start)
        .limit(limitOrder)
        .exec((error, data) => {
            if (error) {
                return res.status(500).json({
                    message: `Loi 500: ${error.message}.`
                });
            } else {
                return res.status(200).json({
                    message: `Lay thong tin thanh cong.`,
                    order: data
                });
            };
        })

}

const getAOrderById = (req, res) => {
    var orderId = req.params.orderId;

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: "Loi 400: Id Order khong dung."
        })
    }

    orderModel.findById(orderId)
        .populate(
            { path: 'orderDetail', model: orderDetailModel, populate: { path: 'product', model: productModel, select: "name" } }
        )
        .populate(
            { path: 'customer', model: customerModel, select: "fullName" }
        )
        .exec((error, data) => {
            if (error) {
                return res.status(500).json({
                    message: `Loi 500: ${error.message}.`
                });
            } else {
                return res.status(200).json({
                    message: `Lay thong tin thanh cong.`,
                    order: data
                });
            }
        });
};

const putAOrderById = (req, res) => {
    var orderId = req.params.orderId;

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: "Loi 400: Id Order khong dung."
        })
    }

    var body = req.body;

    var order = {
        shippedDate: body.shippedDate,
        status: body.status,
        note: body.note,
    }

    orderModel.findByIdAndUpdate(orderId, order, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(200).json({
                message: `Sua thong tin thanh cong.`,
                order: data
            });
        };
    });
};

const deleteAOrderById = (req, res) => {
    var customerId = req.params.customerId;

    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            message: "Loi 400: Id Customer khong dung."
        })
    };

    var orderId = req.params.orderId;

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: "Loi 400: Id Order khong dung."
        })
    }

    orderModel.findByIdAndDelete(orderId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            customerModel.findByIdAndUpdate(customerId,
                {
                    $pull: { order: orderId }
                },
                (err) => {
                    if (err) {

                    } else {
                        return res.status(204).json({
                            message: `Xoa Order thanh cong.`,
                            Order: data
                        });
                    }
                }
            )
        };
    });
};

module.exports = {
    postAOrderOfCustomer,
    getAllOrderOfCustomer,
    getAllOrder,
    getAOrderById,
    putAOrderById,
    deleteAOrderById
};
