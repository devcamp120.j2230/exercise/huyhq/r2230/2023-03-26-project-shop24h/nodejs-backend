const jwt = require('jsonwebtoken');

const authorization = (req, res)=>{
    const token = req.header('auth-token');
    
    if (!token) return res.status(401).json({
        error: `Error 401.`,
        message: `Access Denied!`
    });

    try {
        const verified = jwt.verify(token, 'admin');

        return res.status(200).json({
            message: `Access Accept!`,
            data: verified
        });

    } catch (err) {
        return res.status(400).json({
            error: `Error 400.`,
            message: `User is not access!`
        });
    }
}

module.exports = {authorization}