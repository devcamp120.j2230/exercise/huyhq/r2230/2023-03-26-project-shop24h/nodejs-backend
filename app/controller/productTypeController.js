const { default: mongoose } = require("mongoose");

const productType = require("../model/ProductType");

const postAProductType = (req, res) => {
    var body = req.body;
    if (!body.name) {
        return res.status(400).json({
            message: "Loi 400: Ten san pham phai bat buoc."
        })
    }

    var newProductType = new productType({
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description
    })

    productType.create(newProductType, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(201).json({
                message: `Tao moi san pham thanh cong.`,
                productType: data
            });
        };
    });
};

const getAllProductType = (req, res) => {
    productType.find((error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(200).json({
                message: `Lay san pham thanh cong.`,
                productType: data
            });
        };
    })
};

const getAProductTypeById = (req, res) => {
    var id = req.params.id;
    console.log(id);
   
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "Loi 400: Id san pham khong dung."
        })
    }

    productType.findById(id, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(200).json({
                message: `Lay san pham thanh cong.`,
                productType: data
            });
        };
    })
};

const putAProductTypeById = (req, res) => {
    var id = req.params.id;
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "Loi 400: Id san pham khong dung."
        })
    };

    var body = req.body;
    if (!body.name) {
        return res.status(400).json({
            message: "Loi 400: Ten san pham phai bat buoc."
        })
    };
    
    var product = new productType({
        name: body.name,
        description: body.description
    })

    productType.findByIdAndUpdate(id, product, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(200).json({
                message: `Sua san pham thanh cong.`,
                productType: data
            });
        };
    });
};

const deleteAProductTypeById = (req, res) => {
    var id = req.params.id;
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "Loi 400: Id san pham khong dung."
        })
    };
    productType.findByIdAndDelete(id, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(204).json({
                message: `Xoa san pham thanh cong.`,
                productType: data
            });
        };
    });
};

module.exports = {
    postAProductType,
    getAllProductType,
    getAProductTypeById,
    putAProductTypeById,
    deleteAProductTypeById
};
