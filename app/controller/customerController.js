const { default: mongoose } = require("mongoose");
const customerModel = require("../model/Customer");
const groupModel = require("../model/Group");

const { passwordHash, passwordVerify, } = require('nodejs-password');
const { generateSalt } = require('nodejs-password/lib/helpers');

const generatePassword = async (password, res) => {
    try {
        const salt = await generateSalt(16);
        const hash = await passwordHash(password, salt);
        return { salt, hash }
        // Store hash in your password DB.
    } catch (error) {
        return res.status(500).json({
            message: "Loi 500: " + error.message
        })
    }
}


const postACustomer = async (req, res) => {
    var body = req.body;
    if (!body.fullName) {
        return res.status(400).json({
            message: "Loi 400: Ten khach hang phai bat buoc."
        })
    };

    if (!body.phone) {
        return res.status(400).json({
            message: "Loi 400: So dien thoai phai bat buoc."
        })
    };

    if (!body.email) {
        return res.status(400).json({
            message: "Loi 400: Email phai bat buoc."
        })
    };

    if (!body.password) {
        return res.status(400).json({
            message: "Loi 400: Password phai bat buoc."
        })
    };

    const getHash = await generatePassword(body.password, res);

    var newCustomer = new customerModel({
        _id: mongoose.Types.ObjectId(),
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        imageAvatar: body.imageAvatar,
        address: body.address,
        city: body.city,
        country: body.country,
        district: body.district,
        group: body.group,
        dateCreated: Date.now(),
        salt: getHash.salt,
        hash: getHash.hash,
    })

    customerModel.create(newCustomer, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(201).json({
                message: `Tao khach hang moi thanh cong.`,
                customer: data
            });
        };
    });
};

const getAllCustomer = (req, res) => {
    var start = req.query.start;
    var limitUser = req.query.limit;

var order = req.query.order;
var orderBy = req.query.orderBy;

	//thu thập dữ liệu trên front-end
	let allCondition = req.query.all;
    let nameCustomer = req.query.name;
let emailCustomer = req.query.email;
let phoneCustomer = req.query.phone;

	//tạo ra điều kiện lọc
    let condition = {};
	
if (allCondition) {
        condition = {
		$or:[
		{fullName: { $regex: allCondition}},
		{email: { $regex: allCondition}},
		{phone: { $regex: allCondition}}
]};
}

    if (nameCustomer) {
        condition.fullName = { $regex: nameCustomer };
    }

if (emailCustomer) {
        condition.email = { $regex: emailCustomer };
    }

if (phoneCustomer) {
        condition.phone = { $regex: phoneCustomer };
    }

var sort = "";
	sort = order ? {[`${orderBy}`]: order} : {dateUpdated: 'desc'}

    customerModel.find({...condition, isDeleted: false})
        .populate({ path: 'group', model: groupModel, select: "name"})
        .sort(sort)
        .skip(start)
        .limit(limitUser)
        .exec((error, data) => {
            if (error) {
                return res.status(500).json({
                    message: `Loi 500: ${error.message}.`
                });
            } else {
                return res.status(200).json({
                    message: `Lay thong tin thanh cong.`,
                    customer: data
                });
            };
        })
};

const getACustomerById = (req, res) => {
    var id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Loi 400: Id khach hang khong dung."
        })
    }

    customerModel.findById(id)
        .populate({ path: 'group', model: groupModel, select: "name" })
        .exec((error, data) => {
            if (error) {
                return res.status(500).json({
                    message: `Loi 500: ${error.message}.`
                });
            } else {
                return res.status(200).json({
                    message: `Lay thong tin thanh cong.`,
                    customer: data
                });
            };
        })
};

const putACustomerById = async (req, res) => {
    var id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Loi 400: Id khach hang khong dung."
        })
    };

    var body = req.body;
    if (!body.fullName) {
        return res.status(400).json({
            message: "Loi 400: Ten khach hang phai bat buoc."
        })
    };

    if (!body.phone) {
        return res.status(400).json({
            message: "Loi 400: So dien thoai phai bat buoc."
        })
    };

    if (!body.email) {
        return res.status(400).json({
            message: "Loi 400: Email phai bat buoc."
        })
    };


    var customer = {
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        imageAvatar: body.imageAvatar,
        address: body.address,
        city: body.city,
        country: body.country,
        district: body.district,
        group: body.group,
	dateUpdated: Date.now(),
    }

    if (body.password) {
        const getHash = await generatePassword(body.password, res);
        customer = { ...customer, salt: getHash.salt, hash: getHash.hash }
    };

    customerModel.findByIdAndUpdate(id, customer, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(200).json({
                message: `Sua thong tin thanh cong.`,
                customer: data
            });
        };
    });
};

const deleteACustomerById = (req, res) => {
    var id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Loi 400: Id khach hang khong dung."
        })
    };
    customerModel.findByIdAndDelete(id, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(204).json({
                message: `Xoa khach hang thanh cong.`,
                Customer: data
            });
        };
    });
};

const softDeleteCustomer = async (req, res) => {
    var id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Loi 400: Id khach hang khong dung."
        })
    };

    var customer = {
        isDeleted: true,
        deletedAt: Date.now()
    }

    customerModel.findByIdAndUpdate(id, customer, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(200).json({
                message: `Xoa tam thong tin thanh cong.`,
                customer: data
            });
        };
    });
};

const removeSoftDeleteCustomer = async (req, res) => {
    var id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Loi 400: Id khach hang khong dung."
        })
    };

    var customer = {
        isDeleted: false,
        deletedAt: null,
    }

    customerModel.findByIdAndUpdate(id, customer, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(200).json({
                message: `Xu ly thong tin thanh cong.`,
                customer: data
            });
        };
    });
};

module.exports = {
    postACustomer,
    getAllCustomer,
    getACustomerById,
    putACustomerById,
    deleteACustomerById,
    softDeleteCustomer,
    removeSoftDeleteCustomer
};
