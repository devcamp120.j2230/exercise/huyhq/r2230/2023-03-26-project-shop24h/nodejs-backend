const { default: mongoose } = require("mongoose");
const customerModel = require("../model/Customer");
const groupModel = require("../model/Group");

const { passwordHash, passwordVerify, } = require('nodejs-password');
const { generateSalt } = require('nodejs-password/lib/helpers');

const jwt = require('jsonwebtoken');


const generatePassword = async (password, res) => {
    try {
        const salt = await generateSalt(16);
        const hash = await passwordHash(password, salt);
        return { salt, hash }
        // Store hash in your password DB.
    } catch (error) {
        return res.status(500).json({
            message: "Loi 500: " + error.message
        })
    }
}

const checkPassword = async (password, hash, salt) => {
    const samePassword = await passwordVerify(password, hash, salt); // return true
    return samePassword;
}

const loginUser = (req, res) => {
    var body = req.body;

    if (!body.email) {
        return res.status(400).json({
            message: "Loi 400: Email phai bat buoc."
        })
    };

    if (!body.password) {
        return res.status(400).json({
            message: "Loi 400: Password phai bat buoc."
        })
    };

    customerModel
        .findOne({ email: body.email })
        .populate({ path: 'group', model: groupModel, select: "name" })
        .exec(async (error, data) => {
            if (error) {
                return res.status(500).json({
                    message: `Loi 500: ${error.message}.`
                });
            } else {
                if (data !== null) {
                    const check = await checkPassword(body.password, data.hash, data.salt)
                    // const privateKey = fs.readFileSync('private.key');
                    const token = jwt.sign(
                        { _id: data._id },
                        data.group.name,
                        { expiresIn: 60 * 60 * 24 }
                    );
                    if (check) {
                        return res.status(201).json({
                            message: `Lay du lieu thanh cong.`,
                            Customer: data,
                            token: token
                        });
                    } else {
                        return res.status(400).json({
                            message: `Sai Email hoac Password.`,
                        });
                    }
                } else {
                    return res.status(400).json({
                        message: `Sai Email hoac Password.`,
                    });
                }

            }
        })
};

const updateUser = (req, res) => {
    var body = req.body;

    if (!body.email) {
        return res.status(400).json({
            message: "Loi 400: Email phai bat buoc."
        })
    };

    customerModel
        .findOne({ email: body.email })
        .populate({ path: 'group', model: groupModel, select: "name" })
        .exec(async (error, data) => {
            if (error) {
                return res.status(500).json({
                    message: `Loi 500: ${error.message}.`
                });
            } else {
                if (data !== null) {
                    const token = jwt.sign(
                        { _id: data._id },
                        data.group.name,
                        { expiresIn: 60 * 60 * 24 }
                    );
                        return res.status(201).json({
                            message: `Lay du lieu thanh cong.`,
                            Customer: data,
                            token: token
                        });

                } else {
                    return res.status(400).json({
                        message: `Sai Email hoac Password.`,
                    });
                }

            }
        })
};

const logoutUser = async (req, res) => {

};

const registerNewUser = async (req, res) => {
    var body = req.body;
    if (!body.fullName) {
        return res.status(400).json({
            message: "Loi 400: Ten khach hang phai bat buoc."
        })
    };

    if (!body.phone) {
        return res.status(400).json({
            message: "Loi 400: So dien thoai phai bat buoc."
        })
    };

    if (!body.email) {
        return res.status(400).json({
            message: "Loi 400: Email phai bat buoc."
        })
    };

    if (!body.password) {
        return res.status(400).json({
            message: "Loi 400: Password phai bat buoc."
        })
    };

    const getHash = await generatePassword(body.password, res);

    var newCustomer = new customerModel({
        _id: mongoose.Types.ObjectId(),
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        salt: getHash.salt,
        hash: getHash.hash,
    })

    customerModel.create(newCustomer, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(201).json({
                message: `Tao khach hang moi thanh cong.`,
                Customer: data
            });
        };
    });
};

module.exports = {
    registerNewUser,
    loginUser,
updateUser,
    logoutUser
}