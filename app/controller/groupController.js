const { default: mongoose } = require("mongoose");

const groupModel = require("../model/Group");

const postAGroup = (req, res) => {
    var body = req.body;
    if (!body.name) {
        return res.status(400).json({
            message: "Loi 400: Ten nhom phai bat buoc."
        })
    }

    var newGroup = new groupModel({
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description
    })

    groupModel.create(newGroup, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(201).json({
                message: `Tao moi nhom thanh cong.`,
                group: data
            });
        };
    });
};

const getAllGroup = (req, res) => {
    groupModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(200).json({
                message: `Lay du lieu thanh cong.`,
                group: data
            });
        };
    })
};

const getAGroupById = (req, res) => {
    var id = req.params.id;
   
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "Loi 400: Id Group khong dung."
        })
    }

    groupModel.findById(id, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(200).json({
                message: `Lay san pham thanh cong.`,
                group: data
            });
        };
    })
};

const putAGroupById = (req, res) => {
    var id = req.params.id;
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "Loi 400: Id Group khong dung."
        })
    };

    var body = req.body;
    if (!body.name) {
        return res.status(400).json({
            message: "Loi 400: Ten Group phai bat buoc."
        })
    };
    
    var group = {
        name: body.name,
        description: body.description
    }

    groupModel.findByIdAndUpdate(id, group, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(200).json({
                message: `Sua du lieu thanh cong.`,
                group: data
            });
        };
    });
};

const deleteAGroupById = (req, res) => {
    var id = req.params.id;
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "Loi 400: Id Group khong dung."
        })
    };

    groupModel.findByIdAndDelete(id, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(204).json({
                message: `Xoa du lieu thanh cong.`,
                group: data
            });
        };
    });
};

module.exports = {
    postAGroup,
    getAllGroup,
    getAGroupById,
    putAGroupById,
    deleteAGroupById
};
